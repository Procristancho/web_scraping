import requests
import bs4

from common import config

class Homepage:
    def __init__(self, news_site_uid, url):
        self._config = config()['news_sites'][news_site_uid]
        self._queries = self._config['queries']
        self.html = None

        self._visit(url)

    @property
    def article_links(self):
        link_list = []
        for link in self._select(self._queries['homepage_article_links']):
            if link and link.has_attr('href'):
                link_list.append(link)

        return set(link['href'] for link in link_list)



    def _select(self, query_string):
        return self.html.select(query_string)

    def _visit(self, url):
        response = requests.get(url)

        response.raise_for_status() # devuelve un error si la solicitud no fue concluida correctamente
        self.html = bs4.BeautifulSoup(response.text, 'html.parser')
